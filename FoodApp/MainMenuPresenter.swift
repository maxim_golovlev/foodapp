//
//  MainMenuPresenter.swift
//  FoodApp
//

import UIKit

protocol MainMenuView: class, BaseView {
    func display(_ categories: Categories)
}

class MainMenuPresenter {
    
    weak var view: MainMenuView?
    
    var items: [Item] = [Item]() {
        didSet {
            let categories = Categories(objects: items,
                                        count: items.count)
            view?.display(categories)
        }
    }
    
    func requestData() {
        view?.startLoading()
        
        ScopeManager.fetchCatalog()
            .then { (catalog) -> Void in
                
                let categories = Categories(objects: catalog.items,
                                            count: catalog.items.count,
                                            title: "Меню (\(catalog.items.count))")
                
                self.view?.display(categories)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
}
