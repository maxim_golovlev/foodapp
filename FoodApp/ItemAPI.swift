//
//  ItemAPI.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 15.03.17.
//  Copyright © 2017 Захаров Вячеслав. All rights reserved.
//

import Foundation

class ItemAPI: MainAPI {
    
    static func getItem(byId id: String, parentId: String?, completion: ServerResult?) {
        
        var parameters = [
            "id": id,
            ] as [String : Any]
        
        if parentId != nil {
            parameters["parent"] = parentId
        }
        
        sendRequest(type: .get, url: API.item, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    static func getItems(forCategotyId id: Int?, completion: ServerResult?) {
        
        guard let id = id else { return }
        
        let parameters = ["parent": id] as [String : Any]

        sendRequest(type: .get, url: API.scopeItems , parameters: parameters as [String: AnyObject], headers: nil, completion: completion)
    }
}
