//
//  BaseView.swift
//  24HourTV
//

import Foundation

protocol BaseView: class, Alertable, Loadable { }
