//
//  ItemListSectionController.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright © 2017 Захаров Вячеслав. All rights reserved.
//

import Foundation
import IGListKit
import SDWebImage

class ItemList: NSObject {
    var objects: [Item]?
    
    init(objects: [Item]) {
        self.objects = objects
    }
}

extension ItemList: IGListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}

final class ItemListSectionController: IGListSectionController, IGListSectionType {
    
    var itemList: ItemList?
    
    override init() {
        super.init()
        
        self.minimumInteritemSpacing = 6
        self.minimumLineSpacing = 6
        self.inset = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
    }
    
    func numberOfItems() -> Int {
        return itemList?.objects?.count ?? 0
    }
    
    func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let itemSize = floor(width / 2) - self.minimumInteritemSpacing - 12
        let height = itemSize + 83
        return CGSize(width: itemSize, height: height)
    }
    
    func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "ItemCell", bundle: nil, for: self, at: index) as! ItemCell
        let item = itemList?.objects?[index]
        
        cell.descriptionLabel.text = item?.name
        cell.photoImage.sd_setImage(with: URL(string: item?.photos.first?.url ?? ""), placeholderImage: UIImage())
        cell.price = item?.price
        cell.item = item
        
        cell.initCounter = AppCart.sharedCart.model(forItem: item)?.count
        return cell
    }
    
    func didUpdate(to object: Any) {
        self.itemList = object as? ItemList
    }
    
    func didSelectItem(at index: Int) {
        if let item = itemList?.objects?[index] {
            let itemVc = StoryboardScene.Item.instantiateItemViewController()
            itemVc.presenter.item = item
            viewController?.navigationController?.show(itemVc, sender: nil)
        }
    }
}
