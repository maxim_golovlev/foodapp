//
//  MainPresenter.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit
import IGListKit

protocol MainView: class, BaseView {
    func display(_ data: [IGListDiffable])
}

class ClubCard: NSObject {
    let frontImageUrl: String?
    let backImageUrl: String?
    let loyaltySid: String?
    let loyaltyUrl: String?
    
    init(frontImageUrl: String?, backImageUrl: String?, loyaltySid: String?, loyaltyUrl: String?) {
        self.frontImageUrl = frontImageUrl
        self.backImageUrl = backImageUrl
        self.loyaltySid = loyaltySid
        self.loyaltyUrl = loyaltyUrl
    }
}

extension ClubCard: IGListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}

class MainPresenter {
    
    weak var view: MainView?
    
    func requestData() {
        view?.startLoading()
        
        ScopeManager.fetchCatalog()
            .then { (catalog) -> Void in
            
                let params = catalog.info?.params
                let card = ClubCard(frontImageUrl: params?.cardFrontUrl,
                                    backImageUrl: params?.cardBackUrl,
                                    loyaltySid: params?.loyaltySid,
                                    loyaltyUrl: params?.loyaltyUrl)
                
                let categories = Categories(objects: catalog.items,
                                            count: 2,
                                            title: "Меню (\(catalog.items.count))")
                
                let news = NewsData(title: "Новости и акции (\(catalog.info?.news.count ?? 0))",
                    objects: catalog.info?.news ?? [NewsItem]())
                
                self.view?.display([card, categories, news])
            }
            .always {
                self.view?.stopLoading()
            }
        .catch { (error) in
            if case let ResponseError.withMessage(msg) = error {
                self.view?.showAlert(title: nil, message: msg)
            }
        }
    }
}
