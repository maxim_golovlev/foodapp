//
//  MainMenuViewController.swift
//  FoodApp
//


import UIKit
import IGListKit

class MainMenuViewController: UIViewController {
    
    var presenter: MainMenuPresenter!
    var router: MainMenuRouter!
    
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var data = [IGListDiffable]()
    var filterString = ""
    let searchToken: NSNumber = 42
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        MainMenuConfigurator.sharedInstance.configure(viewController: self)
        collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        setupNavBar()
        
        view.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        
        if isRootVC {
            self.presenter.requestData()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupNavBar() {
        let cartButton = CartBarButton()
        cartButton.didTap = { () in
            self.router.presentCart()
        }
        navigationItem.rightBarButtonItem = cartButton
        
        let sideBarButton = SideBarButton()
        sideBarButton.didTap = { () in
            self.openLeftMenu()
        }
        
        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = sideBarButton
        }
    }
}

extension MainMenuViewController: IGListAdapterDataSource {
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        
        if filterString != "", let items = (data as? [Categories])?.first?.objects  {
            let filtered = items.filter { $0.name!.contains(filterString)}
            return [searchToken, Categories(objects: filtered,
                                            count: filtered.count)]
        } else {
            return [searchToken] + data
        }
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if let obj = object as? NSNumber, obj == searchToken {
            let sectionController = SearchSectionController()
            sectionController.delegate = self
            return sectionController
        } else {
            return CategorySectionController(withType: .withoutHeader)
        }
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
}

extension MainMenuViewController: MainMenuView {
    
    func display(_ categories: Categories) {
        data = [categories]
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension MainMenuViewController: SearchSectionControllerDelegate {
    
    func searchSectionController(_ sectionController: SearchSectionController, didChangeText text: String) {
        filterString = text
        adapter.performUpdates(animated: true, completion: nil)
    }
}

