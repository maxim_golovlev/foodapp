import UIKit
import TableKit

class SelfSizedDescriptionCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with title: String) {
        titleLabel.text = title
    }
}
