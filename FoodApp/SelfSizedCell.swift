import UIKit
import TableKit

class SelfSizedCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String, font: UIFont)) {
        titleLabel.text = data.title
        titleLabel.font = data.font
    }
}
