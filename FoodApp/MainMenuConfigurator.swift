//
//  MainMenuConfigurator.swift
//  FoodApp
//

import UIKit

// MARK: - Connect View and Presenter

class MainMenuConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = MainMenuConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: MainMenuViewController) {
        
        let router = MainMenuRouter()
        router.viewController = viewController
        
        let presenter = MainMenuPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
