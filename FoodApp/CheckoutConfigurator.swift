import UIKit

// MARK: - Connect View and Presenter

class CheckoutConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = CheckoutConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: CheckoutViewController) {
        
        let router = CheckoutRouter()
        router.viewController = viewController
        
        let presenter = CheckoutPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
