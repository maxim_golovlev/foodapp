import UIKit
import IGListKit
import SDWebImage

class NewsData: NSObject {
    var title: String?
    var objects: [NewsItem]
    
    init(title: String?, objects: [NewsItem]) {
        self.title = title
        self.objects = objects
    }
}

extension NewsData: IGListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}


final class NewsSectionController: IGListSectionController, IGListSectionType {
    
    var data: NewsData?
    var type: SectionControllerType?
    
    init(withType type: SectionControllerType = .withHeader) {
        super.init()

        self.inset = UIEdgeInsets(top: 12.5, left: 9.5, bottom: 12.5, right: 9.5)
        supplementaryViewSource = self
        self.type = type
    }
    
    func numberOfItems() -> Int {
        return data?.objects.count ?? 0
    }
    
    func sizeForItem(at index: Int) -> CGSize {
        let width = (collectionContext?.containerSize.width ?? 0) - 19
        return CGSize(width: width, height: 107)
    }
    
    func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "NewsListCell", bundle: nil, for: self, at: index) as! NewsListCell
        let item = data?.objects[index]
        
        cell.titleLabel.text = item?.title
        cell.photoImage.sd_setImage(with: URL(string: item?.image ?? ""), placeholderImage: UIImage())
        
        return cell
    }
    
    func didUpdate(to object: Any) {
        self.data = object as? NewsData
    }
    
    func didSelectItem(at index: Int) {
        showNewsItemVC(item: self.data?.objects[index])
    }
    
    func showNewsItemVC(item: NewsItem?) {
        
        if let item = item {
            
            let nav = StoryboardScene.NewsItem.initialViewController()
            
            if let vc = nav.viewControllers.first as? NewsItemController {
                vc.presenter.newsItem = item
                self.viewController?.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    func showNewsVC() {
        if let items = self.data?.objects {
            let menuVc = StoryboardScene.News.instantiateNewsViewController()
            menuVc.presenter.news = items
            self.viewController?.navigationController?.show(menuVc, sender: nil)
        }
    }
}

extension NewsSectionController: IGListSupplementaryViewSource {
    
    func supportedElementKinds() -> [String] {
        guard let type = self.type else { return []}
        
        switch type {
        case .withHeader:
            return [UICollectionElementKindSectionHeader]
        case .withoutHeader:
            return []
        }
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                       for: self,
                                                                       nibName: "BlockHeaderView",
                                                                       bundle: nil,
                                                                       at: index) as! BlockHeaderView
        view.titleLabel.text = data?.title
        view.tapOn = {
            self.showNewsVC()
        }
        
        return view
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        let height: CGFloat = data?.title != nil ? 46.5 : 0
        return CGSize(width: collectionContext!.containerSize.width, height: height)
    }
    
}
