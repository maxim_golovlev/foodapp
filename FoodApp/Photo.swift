//
//  Photo.swift
//  FoodApp
//

import Foundation
import ObjectMapper

class Photo: NSObject, Mappable {
    var white: Int = 0
    var url: String?
    var thumbnailUrl: String?
    var bigUrl: String?
    var smallUrl: String?
    var previewUrl: String?
    var dimension: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        white            <- map["white"]
        url              <- map["url"]
        thumbnailUrl     <- map["thumbnail_url"]
        bigUrl           <- map["big_url"]
        smallUrl         <- map["small_url"]
        previewUrl       <- map["preview_url"]
        dimension        <- map["dimension"]
    }
}
