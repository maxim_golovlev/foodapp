
import UIKit
import IGListKit

class FavoriteViewController: UIViewController {
    
    @IBOutlet weak var filterButton: UIButton! {
        didSet {
            filterButton.backgroundColor = AppColors.buttonsColor
        }
    }
    
    var presenter: FavoritePresenter!
    var router: FavoriteRouter!
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var data = [IGListDiffable]()
    
    let test = ItemList(objects: [Item(JSON: [ "id": 3865599,
                                               "type": 0,
                                               "name": "Суши",
                                               "update": "2017-02-06 18:57:32",
                                               "custom_filter": 0,
                                               "child": "",
                                               "photos": [
                                                [
                                                    "white": 0,
                                                    "url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847.png",
                                                    "thumbnail_url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847_thumb.png",
                                                    "big_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "small_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "preview_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "dimension": "400*400"
                                                ]
        ],
                                               "count": 21])!,
                                  Item(JSON: [ "id": 3865599,
                                               "type": 0,
                                               "name": "Суши",
                                               "update": "2017-02-06 18:57:32",
                                               "custom_filter": 0,
                                               "child": "",
                                               "photos": [
                                                [
                                                    "white": 0,
                                                    "url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847.png",
                                                    "thumbnail_url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847_thumb.png",
                                                    "big_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "small_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "preview_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "dimension": "400*400"
                                                ]
                                    ],
                                               "count": 21])!,
                                  Item(JSON: [ "id": 3865599,
                                               "type": 0,
                                               "name": "Суши",
                                               "update": "2017-02-06 18:57:32",
                                               "custom_filter": 0,
                                               "child": "",
                                               "photos": [
                                                [
                                                    "white": 0,
                                                    "url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847.png",
                                                    "thumbnail_url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847_thumb.png",
                                                    "big_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "small_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "preview_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "dimension": "400*400"
                                                ]
                                    ],
                                               "count": 21])!,
                                  Item(JSON: [ "id": 3865599,
                                               "type": 0,
                                               "name": "Суши",
                                               "update": "2017-02-06 18:57:32",
                                               "custom_filter": 0,
                                               "child": "",
                                               "photos": [
                                                [
                                                    "white": 0,
                                                    "url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847.png",
                                                    "thumbnail_url": "http://market.arcanite.ru/assets/photos/2017/02/06/9c5445c99a4d80c6fe69cb30a01b5847_thumb.png",
                                                    "big_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "small_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "preview_url": "http://sempai.su/components/com_virtuemart/shop_image/product/_________________5820dbe81d5ce.png",
                                                    "dimension": "400*400"
                                                ]
                                    ],
                                               "count": 21])!])
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FavoriteConfigurator.sharedInstance.configure(viewController: self)
        collectionView.register(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        setupNavBar()
        
        view.addSubview(collectionView)
        view.sendSubview(toBack: collectionView)
        
        adapter.collectionView = collectionView
        adapter.dataSource = self
        display(test)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    func setupNavBar() {
        let cartButton = CartBarButton()
        cartButton.didTap = { () in
            self.router.presentCart()
        }
        navigationItem.rightBarButtonItem = cartButton
        
        let sideBarButton = SideBarButton()
        sideBarButton.didTap = { () in
            self.openLeftMenu()
        }
        navigationItem.leftBarButtonItem = sideBarButton
    }
    
    @IBAction func didTapFilter(_ sender: Any) {
        router.presentFilter()
    }
}

extension FavoriteViewController: FavoriteView {
    
    func display(_ list: ItemList) {
        
        data = [list]
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension FavoriteViewController: IGListAdapterDataSource {
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return data
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        return ItemListSectionController()
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
}

