//
//  ItemRouter.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 15.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

class ItemRouter {
    
    weak var viewController: ItemViewController!
    
    // MARK: - Navigation
    
    func presentCart() {
        let vc = StoryboardScene.Cart.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
}
