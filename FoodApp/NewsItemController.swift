//
//  NewsItemController.swift
//  FoodApp
//
//  Created by Admin on 18.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import UIKit
import TableKit

class NewsItemController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
            tableView.tableFooterView = UIView()
        }
    }
    
    var tableDirector: TableDirector!
    var presenter: NewsItemPresenter!
  //  var router: NewsItemRouter!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NewsItemConfigurator.sharedInstance.configure(viewController: self)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationBarUI()
        setupNavBar()
        
        presenter.requestData()
    }
    
    func setupNavBar() {
        let dismissBarButton = DismissBarButton()
        dismissBarButton.didTap = { () in
            self.dismiss(animated: true, completion: nil)
        }
        navigationItem.leftBarButtonItem = dismissBarButton
    }
}

extension NewsItemController: NewsItemView {
    
    func display(_ newsItem: NewsItem) {
        
        tableDirector.clear()
        
        let imageSection = TableSection()
        
        if let companyUrl = newsItem.image {
            let imagesRow = TableRow<ImageTableCell>(item: companyUrl)
            imageSection.append(row: imagesRow)
            tableDirector += imageSection
        }
        
        let titleSection = TableSection()
        if let title = newsItem.title {
            let titleRow = TableRow<SelfSizedCell>(item: (title: title, font: UIFont(font: FontFamily.SFUIDisplay.medium, size: 22)))
            titleSection.append(row: titleRow)
            tableDirector += titleSection
        }
        
        let textSection = TableSection()
        if let text = newsItem.text {
            let textRow = TableRow<SelfSizedCell>(item: (title: text, font: UIFont(font: FontFamily.SFUIDisplay.medium, size: 14)))
            textSection.append(row: textRow)
            tableDirector += textSection
        }
        
        tableDirector.reload()
        
    }
}
