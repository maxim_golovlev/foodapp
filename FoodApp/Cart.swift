//
//  Cart.swift
//  FoodApp
//
//  Created by Admin on 20.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation

class AppCart: NSObject {
    
    static let sharedCart = AppCart()
    var items = [String: CardItemModel]()
    
    func model(forItem item: Item?) -> CardItemModel? {
        
        guard let item = item, let key = item.name else { return nil }
        if let model = items[key] {
            return model
        }
        
        return nil
    }
    
    func model(forName name: String?) -> CardItemModel? {
        
        guard let key = name else { return nil }
        if let model = items[key] {
            return model
        }
        
        return nil
    }
    
    func append(item: Item?) {
        
        guard let item = item, let key = item.name else { return }
        
        if items[key] != nil {
            items[key]?.count += 1
            
        } else {
            items[key] = CardItemModel(title: item.name, iconURL: item.photos.first?.previewUrl, price: item.price, count: 1)
        }
    }
    
    func create(number: Int, item: Item?) {
        
        removeAll(item: item)
        
        for _ in 0..<number {
            append(item: item)
        }
    }
    
    func appendItem(name: String?) {
        
        guard let key = name else { return }
        
        if items[key] != nil {
            items[key]?.count += 1
        }
    }
    
    func remove(item: Item?) {
        
        guard let item = item, let key = item.name else { return }
        
        if items[key] != nil  {
            items[key]?.count -= 1
            if items[key]?.count == 0 {
                items[key] = nil
            }
        }
    }
    
    func removeAll(item: Item?) {
        guard let item = item, let key = item.name else { return }
        items[key] = nil
    }
    
    func removeItem(name: String?) {
        
        guard let key = name else { return }
        
        if items[key] != nil  {
            items[key]?.count -= 1
            if items[key]?.count == 0 {
                items[key] = nil
            }
        }
    }
    
    func totalPrice(forItem item: Item) -> Int? {
        
        guard let key = item.name else { return nil }
        
        if var model = items[key]  {
            return (model.price ?? 0) * model.count
        }
        
        return nil
    }
    
    func totalPrice() -> Int {
        
        var total = 0
        
        for model in Array(items.values) {
            total += (model.price ?? 0) * model.count
        }
        
        return total
    }
    
    func totalCount() -> Int {
        
        var total = 0
        
        for model in Array(items.values) {
            total += model.count
        }
        
        return total
    }
}

struct CardItemModel {
    
    var title: String?
    var iconURL: String?
    var price: Int?
    var count: Int = 0 {
        didSet {
            if count < 0 {
                count = 0
            }
        }
    }
}
