//
//  ItemPresenter.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 15.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

protocol ItemView: class, BaseView {
    func display(_ model: Item)
}

class ItemPresenter {
    
    weak var view: ItemView?
    
    var item: Item?
    
    func requestData() {
        
        guard let item = item else { return }
        
        self.view?.display(item)
    }
}
