//
//  Item.swift
//  FoodApp
//


import Foundation
import ObjectMapper

class NewsItem: NSObject, Mappable {
    var id: Int?
    var date: String?
    var title: String?
    var image: String?
    var text: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id            <- map["id"]
        text          <- map["text"]
        title         <- map["title"]
        date          <- map["date"]
        image         <- map["image"]
    }
}
