//
//  ScopeCatalog.swift
//  FoodApp
//
//  Created by Admin on 17.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation
import ObjectMapper

class ScopeCatalog: Mappable {
    
    var info: Info?
    var items: [Item] = [Item]()
    var lastUpdate: Int?
    var execution: Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        info            <- map["info"]
        items           <- map["items"]
        lastUpdate      <- map["last_update"]
        execution       <- map["execution"]
    }
}
