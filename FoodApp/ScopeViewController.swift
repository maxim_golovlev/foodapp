//
//  ScopeViewController.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit
import IGListKit

class ScopeViewController: UIViewController {
    
    var presenter: ScopePresenter!
    var router: ScopeRouter!
    
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var data = [IGListDiffable]()
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ScopeConfigurator.sharedInstance.configure(viewController: self)
        collectionView.register(UINib(nibName: "ShopCell", bundle: nil), forCellWithReuseIdentifier: "ShopCell")
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        presenter.requestData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
}

extension ScopeViewController: ScopeView {
    func display(_ model: [IGListDiffable]) {
        
        self.data = model
        
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension ScopeViewController: IGListAdapterDataSource {
    
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return data
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        return ShopsSectionController()
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
    
}
