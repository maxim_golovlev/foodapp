import UIKit
import TableKit

class CheckoutViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
        }
    }
    
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.backgroundColor = AppColors.buttonsColor
        }
    }
    
    var presenter: CheckoutPresenter!
    var router: CheckoutRouter!
    var tableDirector: TableDirector!

    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CheckoutConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        setupNavBar()
        display()
    }
    
    func setupNavBar() {
        let dismissBarButton = DismissBarButton()
        dismissBarButton.didTap = { () in
            self.dismiss(animated: true, completion: nil)
        }
        navigationItem.leftBarButtonItem = dismissBarButton
    }
}

extension CheckoutViewController: CheckoutView {
    func display() {
        tableDirector.clear()
        
        
        tableDirector.reload()
    }
}

