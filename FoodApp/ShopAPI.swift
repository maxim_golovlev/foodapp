//
//  ShopAPI.swift
//  FoodApp
//


import Foundation

struct ShopAPI: MainAPI {
    
    static func getShop(byId id: Int, completion: ServerResult?) {
        
        let parameters = [
            "id": id,
            ] as [String : Any]
        
        sendRequest(type: .get, url: API.shop, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
}
