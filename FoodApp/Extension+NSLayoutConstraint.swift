import UIKit

extension NSLayoutConstraint {
    
    @IBInspectable var iPhone5: CGFloat {
        set {
            guard Screen.height == 568 else { return }
            
            constant = newValue
        }
        get {
            return constant
        }
    }
    
    @IBInspectable var iPhone7: CGFloat {
        set {
            guard Screen.height == 667 else { return }
            
            constant = newValue
        }
        get {
            return constant
        }
    }
    
    @IBInspectable var iPhone7Plus: CGFloat {
        set {
            guard Screen.height == 736 else { return }
            
            constant = newValue
        }
        get {
            return constant
        }
    }
}
