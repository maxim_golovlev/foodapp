//
//  ItemListViewController.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit
import IGListKit

class ItemListViewController: UIViewController {
    
    @IBOutlet weak var filterButton: UIButton! {
        didSet {
            filterButton.backgroundColor = AppColors.buttonsColor
        }
    }
    
    var presenter: ItemListPresenter!
    var router: ItemListRouter!
    
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var data = [IGListDiffable]()
    let searchToken: NSNumber = 42
    var filterString = ""

    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ItemListConfigurator.sharedInstance.configure(viewController: self)
        collectionView.register(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        view.sendSubview(toBack: collectionView)
        
        let cartButton = CartBarButton()
        cartButton.didTap = { () in
            self.router.presentCart()
        }
        navigationItem.rightBarButtonItem = cartButton
        
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        filterButton.imageView?.contentMode = .center
        
        presenter.requestData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        adapter.reloadData(completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    @IBAction func didTapFilter(_ sender: Any) {
        router.presentFilter()
    }
}

extension ItemListViewController: IGListAdapterDataSource {
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        
        if filterString != "", let items = (data as? [ItemList])?.first?.objects  {
            let filtered = items.filter { $0.name!.contains(filterString)}
            return [searchToken, ItemList(objects: filtered)]
        } else {
            return [searchToken] + data
        }
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if let obj = object as? NSNumber, obj == searchToken {
            let sectionController = SearchSectionController()
            sectionController.delegate = self
            return sectionController
        } else {
            return ItemListSectionController()
        }
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
}

extension ItemListViewController: ItemListView {
    
    func display(_ list: ItemList) {
        data = [list]
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension ItemListViewController: SearchSectionControllerDelegate {
    
    func searchSectionController(_ sectionController: SearchSectionController, didChangeText text: String) {
        filterString = text
        adapter.performUpdates(animated: true, completion: nil)
    }
}

