import UIKit

extension UIViewController {
    
    func prepareNavigationBarUI() {
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
//        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(font: FontFamily.OpenSans.Regular, size: 12)], for: UIControlState.normal)

        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.shadowImage = AppColors.navBarColor.as1ptImage()
        self.navigationController?.navigationBar.setBackgroundImage(AppColors.navBarColor.as1ptImage(), for: .default)
        
//        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back_button")
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back_button")
    }
    
    func dismissVC() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension UIViewController {
    
    func openLeftMenu() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.sideMenu?.presentLeftMenuViewController()
    }
    
    func showViewController(vc: UIViewController) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.sideMenu?.setContentViewController(vc, animated: true)
    }
    
    func showMainScreen() {
        let vc = StoryboardScene.Main.initialViewController()
        self.showViewController(vc: vc)
    }
    
    var isRootVC: Bool {
        return navigationController?.viewControllers.count == 1
    }
    
    var isModal: Bool {
        
        if self.presentingViewController != nil {
            return true
        }
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController {
            return true
        }
        if let state = self.tabBarController?.presentingViewController?.isKind(of: UITabBarController.self), state == true {
            return true
        }
        return false
    }
}
