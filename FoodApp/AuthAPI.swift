//
//  AuthAPI.swift
//  FoodApp
//

import Foundation
import UIKit

enum Action: String {
    case get
    case set
}

struct AuthAPI: MainAPI {
    
    static func initMethod(action: Action, completion: ServerResult?) {
        
        var parameters = [
            "action": action.rawValue,
            "gen": 1,
            "platform": "IOS" + UIDevice.current.systemVersion
        ] as [String : Any]
        
        if action == .set {
            parameters["stamp"] = UIDevice.current.identifierForVendor!
        }
        
        sendRequest(type: .get, url: API.initMethod, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
}
