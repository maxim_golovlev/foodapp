//
//  MainViewController.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit
import IGListKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var collectionView: IGListCollectionView!
    @IBOutlet weak var clubCardButton: FlippingButton!
    
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.tintColor = AppColors.buttonsColor
        }
    }
    
    var presenter: MainPresenter!
    var router: MainRouter!
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    var data = [IGListDiffable]()

    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        MainConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        collectionView.register(UINib(nibName: "NewsListCell", bundle: nil), forCellWithReuseIdentifier: "NewsListCell")
        prepareNavigationBarUI()
        
        adapter.collectionView = collectionView
        adapter.dataSource = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        button.tintColor = AppColors.buttonsColor
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        presenter.requestData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        prepareNavigationBarUI()        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        openLeftMenu()
    }
}

extension MainViewController: IGListAdapterDataSource {
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return data
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if object is NewsData {
            return NewsSectionController()
        } else {
            return CategorySectionController()
        }
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
}

extension MainViewController: MainView {
    
    func display(_ data: [IGListDiffable]) {
        
        self.data = data
        adapter.performUpdates(animated: true, completion: nil)

        let clubCard = data.filter { $0 is ClubCard }.first as? ClubCard
        
        if let card = clubCard {
            clubCardButton.frontImageUrl = card.frontImageUrl
            clubCardButton.backImageUrl = card.backImageUrl
        }
    }
}

