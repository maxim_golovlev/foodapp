//
//  ScopeShop.swift
//  foodapp
//
//  Created by Admin on 17.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation
import ObjectMapper

class ScopeShop: Mappable {
    
    var id: String?
    var name: String?
    var sid: String?
    var logoURL: String?
    var type: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id      <- map["id"]
        name    <- map["name"]
        sid     <- map["sid"]
        logoURL <- map["logo"]
        type    <- map["type"]
    }
}
