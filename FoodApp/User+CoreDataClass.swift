//
//  User+CoreDataClass.swift
//  FoodApp
//

import Foundation
import MagicalRecord

@objc(User)
public class User: NSManagedObject {

    static var current: User? {
        return User.mr_findFirst()
    }
    
}
