//
//  ItemListRouter.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

class ItemListRouter {
    
    weak var viewController: ItemListViewController!
    
    // MARK: - Navigation
    
    func presentFilter() {
        let vc = StoryboardScene.Filter.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
    
    func presentCart() {
        let vc = StoryboardScene.Cart.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
}
