import UIKit
import TableKit

class ImageTableCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var iconImage: UIImageView!
    
    static var defaultHeight: CGFloat? {
        return Screen.height / 3 + 12
    }
    
    func configure(with imagePath: String) {
        if let url = URL(string: imagePath) {
            iconImage.sd_setImage(with: url)
        }
    }
    
}
