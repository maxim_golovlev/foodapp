//
//  ItemViewController.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 15.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit
import TableKit

class ItemViewController: UIViewController {
    
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var newPriceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
        }
    }
    
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.backgroundColor = AppColors.buttonsColor
        }
    }
    
    var presenter: ItemPresenter!
    var router: ItemRouter!
    var tableDirector: TableDirector!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ItemConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let cartButton = CartBarButton()
        cartButton.didTap = { () in
            self.router.presentCart()
        }
        navigationItem.rightBarButtonItem = cartButton
        
        let attrString = NSAttributedString(string: "650", attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        oldPriceLabel.attributedText = attrString
        
        presenter.requestData()
    }
}

extension ItemViewController: ItemView {
    
    
    func display(_ model: Item) {
        
        tableDirector.clear()
        
        let imageSection = TableSection()
        let imagesRow = TableRow<ImagesCell>(item: model.photos.flatMap{ $0.url } + ["https://media-cdn.tripadvisor.com/media/photo-s/01/90/78/c5/sunset-over-dinner-nice.jpg",
                                                                                     "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTQNGIEo9h6_wRAz0tZlMpcuqS_8Vqqm3e_oqHhew8P39NNc4mCOg",
                                                                                     "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRYtuqCyteDhGCUznJuqGLNFRGo7-8j-yw7kIwpe_tsVSPlVPXr"] )
        imageSection.append(row: imagesRow)
        tableDirector += imageSection
        
        let disclosureSection = TableSection()
        let titleRow = TableRow<SelfSizedCell>(item: (title: model.name ?? "Пицца с мидиями + Кока-Кола 2л в подарок", font: UIFont(font: FontFamily.SFUIDisplay.regular, size: 22)))
        disclosureSection.append(row: titleRow)
        let disclosureRow = TableRow<DisclosureIndicatorCell>(item: (title: "Количество", detail: "1", id: nil))
        disclosureSection.append(row: disclosureRow)
        let disclosureRow1 = TableRow<DisclosureIndicatorCell>(item: (title: "Размер", detail: "30 см", id: nil))
        disclosureSection.append(row: disclosureRow1)
        tableDirector += disclosureSection
        
        let descriptionSection = TableSection()
        let descriptionSectionHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 30))
        let separator = UIView(frame: CGRect(x: 0, y: descriptionSectionHeaderView.frame.height - 0.5, width: view.frame.width, height: 0.5))
        separator.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
        descriptionSectionHeaderView.addSubview(separator)
        descriptionSection.headerView = descriptionSectionHeaderView
        let descriptionRow = TableRow<DescriptionCell>(item: (title: "Вес", detail: "670 гр."))
        descriptionSection.append(row: descriptionRow)
        
        let descriptionRow1 = TableRow<DescriptionCell>(item: (title: "Тип", detail: "Итальянская"))
        descriptionSection.append(row: descriptionRow1)
        tableDirector += descriptionSection
        
        let selfSizedSection = TableSection()
        selfSizedSection.headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 15))
        selfSizedSection.footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 77))
        let selfSizedRow = TableRow<SelfSizedDescriptionCell>(item: model.itemDescription ?? "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")
        selfSizedSection.append(row: selfSizedRow)
        tableDirector += selfSizedSection
        
        tableDirector.reload()
        
    }

}

