
import UIKit
import TableKit

class AboutCompanyViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
        }
    }
    
    var presenter: AboutCompanyPresenter!
    var router: AboutCompanyRouter!
    var tableDirector: TableDirector!

    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AboutCompanyConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.requestData()
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        openLeftMenu()
    }
}

extension AboutCompanyViewController: AboutCompanyView {
    
    func display(_ model: AboutCompanyViewModel) {
        tableDirector.clear()
        
        let imageSection = TableSection()
        
        if let companyUrl = model.logotypeUrl {
            let imagesRow = TableRow<ImageTableCell>(item: companyUrl)
            imageSection.append(row: imagesRow)
            tableDirector += imageSection
        } else {
            let spacerRow = TableRow<SelfSizedCell>(item: (title: "\n\n", font: UIFont(font: FontFamily.SFUIDisplay.medium, size: 22)))
            imageSection.append(row: spacerRow)
            tableDirector += imageSection
        }
        
        let disclosureSection = TableSection()
        
        let titleRow = TableRow<SelfSizedCell>(item: (title: "О нашей компании", font: UIFont(font: FontFamily.SFUIDisplay.medium, size: 22)))
        disclosureSection.append(row: titleRow)
        
        if let contactPhone = model.contactPhone {
            let disclosureRow = TableRow<DisclosureIndicatorCell>(item: (title: "Позвонить", detail: contactPhone, id: nil))
            disclosureSection.append(row: disclosureRow)
        }
        
        if let companyUrl = model.companyUrl {
            let disclosureRow1 = TableRow<DisclosureIndicatorCell>(item: (title: "Сайт", detail: companyUrl, id: nil))
            disclosureSection.append(row: disclosureRow1)
        }
        if let contactEmail = model.contactEmail {
            let disclosureRow2 = TableRow<DisclosureIndicatorCell>(item: (title: "Написать", detail: contactEmail, id: nil))
            disclosureSection.append(row: disclosureRow2)
        }
        
        if let adress = model.addresses?.first?.name {
            let disclosureRow3 = TableRow<DisclosureIndicatorCell>(item: (title: "На карте", detail: adress, id: nil))
            disclosureSection.append(row: disclosureRow3)
        }
        
        tableDirector += disclosureSection
        
        if let contactAddress = model.contactAddress {
            
            let selfSizedSection = TableSection()
            let selfSizedRow = TableRow<SelfSizedClearCell>(item: contactAddress)
            selfSizedSection.append(row: selfSizedRow)
            tableDirector += selfSizedSection
        }
        
        tableDirector.reload()
    }

}

