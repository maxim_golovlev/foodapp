//
//  ItemManager.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 15.03.17.
//  Copyright © 2017 Захаров Вячеслав. All rights reserved.
//

import Foundation
import PromiseKit
import MagicalRecord

struct ItemManager {
    
    static func getItem(byId id: String, parentId: String? = nil) -> Promise<Item> {
        return Promise(resolvers: { (fullfill, reject) in
            
            
            ItemAPI.getItem(byId: id, parentId: parentId, completion: { (response) in
                
                switch response {
                case let .Success(response: data):
                    if let item = Item(JSON: data) {
                        fullfill(item)
                    } else {
                        reject(ResponseError.withMessage(AlertMsg.unknownError))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
    
    static func fetchItems(forCategoryId id: Int?) -> Promise<[Item]> {
        
        return Promise(resolvers: { (fullfill, reject) in
            
            ItemAPI.getItems(forCategotyId: id, completion: { (responce) in
                
                switch responce {
                case let .Success(response: data):
                    if let categoryItems = CategoryItems(JSON: data), let items = categoryItems.items {
                        fullfill(items)
                    } else {
                        reject(ResponseError.withMessage(AlertMsg.unknownError))
                    }

                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
}
