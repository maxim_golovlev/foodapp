import UIKit
import TableKit

class CartViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
        }
    }
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.backgroundColor = AppColors.buttonsColor
        }
    }
    
    var presenter: CartPresenter!
    var router: CartRouter!
    var tableDirector: TableDirector!

    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CartConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        setupNavBar()
        
        presenter.requestData()
        
    }
    
    func setupNavBar() {
        let dismissBarButton = DismissBarButton()
        dismissBarButton.didTap = { () in
            self.dismiss(animated: true, completion: nil)
        }
        navigationItem.leftBarButtonItem = dismissBarButton
    }
    
    @IBAction func didTapCheckout(_ sender: Any) {
        router.presentCheckout()
    }
}

extension CartViewController: CartView {
    
    func display(_ models: [String: CardItemModel]?) {
        
        guard let models = models else { return }
        
        setupTable(models: models)
        
    }
    
    func setupTable(models: [String: CardItemModel]) {
        
        tableDirector.clear()
        
        let action = TableRowAction<CartCell>(.custom(MyActions.ButtonClicked)) { (cell) -> Void in
            
            DispatchQueue.main.async {
                self.presenter.requestData()
            }
        }
        
        let cartSection = cardSection(models: models, action: action)
        tableDirector += cartSection
        
        let descriptionSection = totalSection(models: models)
        tableDirector += descriptionSection
        
        tableDirector.reload()
        
    }
    
    func totalSection(models: [String: CardItemModel]) -> TableSection {
        let descriptionSection = TableSection()
        
        descriptionSection.headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 21))
        
        descriptionSection.footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 65))
        
        let descriptionRow = TableRow<BoldDescriptionCell>(item: (title: "Количество позиций", detail: "\(AppCart.sharedCart.totalCount())"))
        descriptionSection.append(row: descriptionRow)
        
        let descriptionRow1 = TableRow<BoldDescriptionCell>(item: (title: "Итого к оплате", detail: "\(AppCart.sharedCart.totalPrice())"))
        descriptionSection.append(row: descriptionRow1)
        
        return descriptionSection
    }
    
    func cardSection(models: [String: CardItemModel], action: TableRowAction<CartCell>) -> TableSection {
        
        let cartSection = TableSection()
        for model in Array(models.values) {
            let cartRow = TableRow<CartCell>(item: model, actions:[action])
            cartSection.append(row: cartRow)
        }
        return cartSection
        
    }
}

