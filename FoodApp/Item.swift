//
//  Item.swift
//  FoodApp
//


import Foundation
import ObjectMapper

class Item: NSObject, Mappable {
    
    var name: String?
    var price: Int?
    var currency: String?
    var itemDescription: String?
    var allowOrder: Bool?
    var sort: String?
    var id: Int?
    var type: Int?
    var update: String?
    var params: [[String: String]]?
    var photos: [Photo] = [Photo]()
    
    var customFilter: Int?
    var count: Int?
    var child: [Item] = [Item]()
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        name                <- map["name"]
        price               <- map["price"]
        currency            <- map["currency"]
        itemDescription     <- map["description"]
        allowOrder          <- map["allow_order"]
        sort                <- map["sort"]
        id                  <- map["id"]
        type                <- map["type"]
        update              <- map["update"]
        params              <- map["params"]
        photos              <- map["photos"]
        
        customFilter        <- map["custom_filter"]
        count               <- map["count"]
        child               <- map["child"]
    }
}
