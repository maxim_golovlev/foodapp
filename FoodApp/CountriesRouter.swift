import UIKit

class CountriesRouter {
    
    weak var viewController: CountriesViewController!
    
    // MARK: - Navigation
    
    func showScope(shops: [ScopeShop]) {
        let vc = StoryboardScene.Scope.instantiateScopeViewController()
        vc.presenter.shops = shops
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showCategory(models: [CountyViewModel]) {
        let vc = StoryboardScene.Countries.instantiateCountriesViewController()
        viewController.navigationController?.pushViewController(vc, animated: true)
        vc.presenter.models = models
    }
}
