import Foundation
import PromiseKit
import MagicalRecord

struct ScopeManager {
    
    static func getCountires() -> Promise<[Country]> {
        return Promise(resolvers: { (fullfill, reject) in
            
            ScopeAPI.getCountries(completion: { (response) in
                
                switch response {
                case let .Success(response: data):
                    let countries = data.flatMap {
                        return Country(JSON: $0)
                    }
                    
                    fullfill(countries)
                    
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
    
    static func fetchShops(withKey key: String) -> Promise<[ScopeCategory]> {
        
        return Promise(resolvers: { (fullfill, reject) in
            
            ScopeAPI.fetchShops(withKey: key, completion: { (responce) in
                
                switch responce {
                    
                case let .Success(response: data):
                    let coutries = data.flatMap { ScopeCategory(JSON: $0) }
                    
                    fullfill(coutries)
                    
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                    
                }
            })
        })
    }
    
    static func fetchCatalog() -> Promise<ScopeCatalog> {
        
        return Promise(resolvers: { (fullfill, reject) in
            
            guard AppShopSID.stringValue != nil else {
                
                reject(ResponseError.withMessage("Shop not selected"))
                return
            }
            
            ScopeAPI.fetchCatalog(withSID: AppShopSID.stringValue) { (responce) in
                
                switch responce {
                    
                case let .Success(response: data):
                    
                    guard let catalog = ScopeCatalog(JSON: data) else { return }
                    fullfill(catalog)
                    
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                    
                }
            }
        })
    }
}
