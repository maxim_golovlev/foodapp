//
//  CategoryItems.swift
//  FoodApp
//
//  Created by Admin on 18.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation
import ObjectMapper

class CategoryItems: Mappable {
    
    var total: Int?
    var total_merge: Int?
    var title: String?
    var items: [Item]?
    var execution: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total       <- map["total"]
        total_merge <- map["total_merge"]
        title       <- map["title"]
        items       <- map["items"]
        execution   <- map["execution"]
    }
}
