import UIKit
import IGListKit
import SDWebImage

class Categories: NSObject {
    var title: String?
    var objects: [Item]?
    var count: Int
    
    init(objects: [Item], count: Int, title: String? = nil) {
        self.objects = objects
        self.count = count
        self.title = title
    }
}

extension Categories: IGListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}

enum SectionControllerType {
    case withHeader
    case withoutHeader
}


final class CategorySectionController: IGListSectionController, IGListSectionType {

    var categories: Categories?
    var type: SectionControllerType?
    
    init(withType type: SectionControllerType = .withHeader) {
        
        super.init()
        
        self.minimumInteritemSpacing = 6
        self.minimumLineSpacing = 6
        self.inset = UIEdgeInsets(top: 0, left: 12, bottom: 8, right: 12)
        supplementaryViewSource = self
        self.type = type

    }


    func numberOfItems() -> Int {
        return categories?.count ?? 0
    }

    func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let itemSize = floor(width / 2) - self.minimumInteritemSpacing - 12
        return CGSize(width: itemSize, height: itemSize)
    }

    func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "CategoryCell", bundle: nil, for: self, at: index) as! CategoryCell
        let item = categories?.objects?[index]
        
        cell.titleLabel.text = item?.name
        cell.photoImage.sd_setImage(with: URL(string: item?.photos.first?.url ?? ""), placeholderImage: UIImage())
        
        return cell
    }

    func didUpdate(to object: Any) {
        self.categories = object as? Categories
    }

    func didSelectItem(at index: Int) {
        guard let item = categories?.objects?[index] else { return }
        
        let itemListVc = StoryboardScene.ItemList.instantiateItemListViewController()
        itemListVc.presenter.categoryId = item.id
        itemListVc.navigationItem.title = item.name
        viewController?.navigationController?.show(itemListVc, sender: nil)
    }
}

extension CategorySectionController: IGListSupplementaryViewSource {
    
    func supportedElementKinds() -> [String] {
        
        guard let type = self.type else { return []}
        
        switch type {
        case .withHeader:
            return [UICollectionElementKindSectionHeader]
        case .withoutHeader:
            return []
        }
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                       for: self,
                                                                       nibName: "BlockHeaderView",
                                                                       bundle: nil,
                                                                       at: index) as! BlockHeaderView
        view.titleLabel.text = categories?.title
        view.tapOn = { () in
            if let items = self.categories?.objects {
                let menuVc = StoryboardScene.MainMenu.instantiateMainMenuViewController()
                menuVc.presenter.items = items
                self.viewController?.navigationController?.show(menuVc, sender: nil)
            }
        }
        
        return view
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        let height: CGFloat = categories?.title != nil ? 46.5 : 0
        return CGSize(width: collectionContext!.containerSize.width, height: height)
    }

}
