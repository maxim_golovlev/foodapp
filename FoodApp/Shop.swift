//
//  Shop.swift
//  FoodApp
//

import Foundation
import ObjectMapper

class Shop: Mappable {
    var info: Info?
    var items: [Item] = [Item]()
    var lastUpdate: Int?
    var execution: Double?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        info            <- map["info"]
        items           <- map["items"]
        lastUpdate      <- map["last_update"]
        execution       <- map["execution"]
    }
}
