//
//  Extension+Optionals.swift
//  24HourTV
//

import Foundation

extension Optional {

    func then(_ handler: (Wrapped) -> Void) {
        switch self {
        case .some(let wrapped): return handler(wrapped)
        case .none: break
        }
    }
}
