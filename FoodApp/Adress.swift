//
//  Adress.swift
//  FoodApp
//
//  Created by Admin on 17.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation
import ObjectMapper

class Adress: Mappable {
    
    var id: String?
    var name: String?
    var title: String?
    var lat: String?
    var lng: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
        title       <- map["title"]
        lat         <- map["lat"]
        lng         <- map["lng"]
    }
}
