//
//  ShopsSectionController.swift
//  FoodApp
//
//  Created by Anton Vodolazkyi on 10.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation
import IGListKit
import SDWebImage

class ShopsList: NSObject {
    var objects: [ScopeShop]?
    
    init(objects: [ScopeShop]) {
        self.objects = objects
    }
}

extension ShopsList: IGListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}


final class ShopsSectionController: IGListSectionController, IGListSectionType {
    
    var data: ShopsList?
    
    override init() {
        super.init()
        
        self.minimumInteritemSpacing = 6
        self.minimumLineSpacing = 10
        self.inset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
    }
    
    func numberOfItems() -> Int {
        return data?.objects?.count ?? 0
    }
    
    func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let height: CGFloat = 105
        return CGSize(width: width, height: height)
    }
    
    func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "ShopCell", bundle: nil, for: self, at: index) as! ShopCell
        let item = data?.objects?[index]
        cell.titleLabel.text = item?.name
        cell.photoImage.sd_setImage(with: URL(string: item?.logoURL ?? ""), placeholderImage: nil)
        
        return cell
    }
    
    func didUpdate(to object: Any) {
        self.data = object as? ShopsList
    }
    
    func didSelectItem(at index: Int) {
        
        let item = data?.objects?[index]
        AppShopSID.stringValue = item?.sid
        
        
        if let isModal = self.viewController?.isModal, isModal == false {
            self.viewController?.showMainScreen()
        } else {
            self.viewController?.dismiss(animated: true, completion: nil)
        }
    }
}
