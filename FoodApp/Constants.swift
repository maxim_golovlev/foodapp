//
//  Constants.swift
//  W8Monitor
//
//  Copyright © 2016 moleculus. All rights reserved.
//

import Foundation
import UIKit

struct Screen {
    static let scale = UIScreen.main.scale
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    static let pixel = 1 / UIScreen.main.scale
    static let navbarHeight = 64
    static let tabbarHeight = 49
}

struct API {
    static let baseURL = "http://market.arcanite.ru/api/"
    static let scopeURL = "http://scope.arcanite.ru/api/"
    
    static let initMethod = "init"
    static let shop = "shop.json"
    static let item = "item.json"
    static let countries = "countries"
    
    static let scopeShops = "shops"
    static let scopeCatalog = "shop"
    static let scopeItems = "items"
}

struct AlertMsg {
    static let unknownError = "Something goes wrong. Try again later."
}

struct AppShopSID {
    
    private static let sidKey = "app_shop_sid"
    
    static var stringValue: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: sidKey)
            UserDefaults.standard.synchronize()
        }
        get {
            let sid = UserDefaults.standard.string(forKey: sidKey)
            return sid
        }
    }
}
