//
//  OrderTemplate.swift
//  FoodApp
//


import Foundation
import ObjectMapper

class OrderTemplate: Mappable {
    var id: Int?
    var title: String?
    var type: String?
    var required: Bool?
    var sort: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id            <- map["id"]
        title         <- map["title"]
        type          <- map["type"]
        required      <- map["required"]
        sort          <- map["sort"]
    }
}
