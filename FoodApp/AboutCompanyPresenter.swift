
import UIKit

protocol AboutCompanyView: class, BaseView {
    func display(_ model: AboutCompanyViewModel)
}

struct AboutCompanyViewModel {
    
    var companyUrl: String?
    var contactAddress: String?
    var contactEmail: String?
    var contactPhone: String?
    var serviceType: String?
    var logotypeUrl: String?
    var addresses: [Adress]?
}

class AboutCompanyPresenter {
    
    weak var view: AboutCompanyView?
  
    
    func requestData() {
        view?.startLoading()
        
        ScopeManager.fetchCatalog()
            .then { (catalog) -> Void in
                
                let info = catalog.info
                
                let model = AboutCompanyViewModel(companyUrl: info?.companyUrl,
                                                  contactAddress: info?.contactAddress,
                                                  contactEmail: info?.contactEmail,
                                                  contactPhone: info?.contactPhone,
                                                  serviceType: info?.serviceType,
                                                  logotypeUrl: info?.logotypeUrl,
                                                  addresses: info?.addresses)
                
                self.view?.display(model)
                
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
        
    }
  
    
    
    
}
