import UIKit

class CartBarButton: UIBarButtonItem {
    
    var didTap: (() -> ())?
    
    // MARK: - Inits.
    
    override init() {
        super.init()
        
        self.image = #imageLiteral(resourceName: "cart_button")
        self.target = target
        self.action = #selector(didTapOn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CartBarButton {
    
    func didTapOn() {
        didTap?()
    }
}

