import Foundation

enum APIResponse {
    case Success (response: Dictionary<String, AnyObject>)
    case Error (message: String?)
}

typealias ServerResult = (_ response: APIResponse) -> Void

enum APIArrayResponse {
    case Success (response: [Dictionary<String, AnyObject>])
    case Error (message: String?)
}

typealias ServerArrayResult = (_ response: APIArrayResponse) -> Void
