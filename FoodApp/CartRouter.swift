import UIKit

class CartRouter {
    
    weak var viewController: CartViewController!
    
    // MARK: - Navigation
    
    func presentCheckout() {
        let vc = StoryboardScene.Checkout.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
}
