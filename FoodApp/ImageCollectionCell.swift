
import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageToDisplay: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    func setImage(_ path: String) {
        if let url = URL(string: path) {
            activityIndicator.startAnimating()
            
            imageToDisplay.sd_setImage(with: url, completed: { [unowned self] (_, _, _, _) in
                self.activityIndicator.stopAnimating()
            })
        }
    }
    
}
