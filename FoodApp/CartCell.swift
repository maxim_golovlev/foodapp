import UIKit
import TableKit

struct MyActions {
    
    static let ButtonClicked = "ButtonClicked"
}


class CartCell: UITableViewCell, ConfigurableCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var oneBtn: UIButton! {
        didSet{
            oneBtn.layer.borderColor = AppColors.buttonsColor.cgColor
            oneBtn.tintColor = AppColors.buttonsColor
        }
    }
    @IBOutlet weak var secondBtn: UIButton!{
        didSet{
            secondBtn.layer.borderColor = AppColors.buttonsColor.cgColor
            secondBtn.tintColor = AppColors.buttonsColor
        }
    }
    

    var id: String? = nil
    
    static var defaultHeight: CGFloat? {
        return 120
    }
    
    var price: Int? {
        didSet {
            priceLabel.text = "\(price ?? 0)"
        }
    }
    
    var totalPrice: Int? {
        didSet {
            totalPriceLabel.text = "\(totalPrice ?? 0)"
        }
    }
    
    var counter: Int? {
        didSet {
            countLabel.text = "\(counter ?? 0)"
            totalPrice = (price ?? 0) * (counter ?? 1)
        }
    }
    
    func configure(with data: CardItemModel) {
        
        if let imageUrl = data.iconURL, let url = URL(string: imageUrl) {
            photoImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "item_placeholder"))
        }
        
        titleLabel.text = data.title
        self.price = data.price
        self.counter = data.count
    }
    @IBAction func addTapped(_ sender: Any) {        
        AppCart.sharedCart.appendItem(name: titleLabel.text)
        updateCart()
    }
    
    @IBAction func removeTapped(_ sender: Any) {
        AppCart.sharedCart.removeItem(name: titleLabel.text)
        updateCart()
    }
    
    func updateCart() {
      //  counter = AppCart.sharedCart.model(forName: titleLabel.text)?.count
        TableCellAction(key: MyActions.ButtonClicked, sender: self).invoke()
    }
    
}
