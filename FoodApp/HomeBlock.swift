//
//  HomeBlock.swift
//  FoodApp
//


import Foundation
import ObjectMapper

class HomeBlock: Mappable {
    var block: String?
    var sort: Int?
    var active: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        block        <- map["block"]
        sort         <- map["sort"]
        active       <- map["active"]
    }
}
