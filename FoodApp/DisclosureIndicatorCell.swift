
import UIKit
import TableKit

class DisclosureIndicatorCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    var id: String? = nil
    
    static var defaultHeight: CGFloat? {
        return 47.5
    }
    
    func configure(with data: (title: String, detail: String, id: String?)) {
        titleLabel.text = data.title
        detailLabel.text = data.detail
        id = data.id
    }
}
