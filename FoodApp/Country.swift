import Foundation
import ObjectMapper

class Country: Mappable {
    
    var id: String?
    var code: String?
    var title: String?
    var count: Int?
    var description: String?
    var sort: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id               <- map["id"]
        code             <- map["code"]
        title            <- map["title"]
        count            <- map["count"]
        description      <- map["description"]
        sort             <- map["sort"]
    }
}
