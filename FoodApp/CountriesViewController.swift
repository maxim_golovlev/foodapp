import UIKit
import TableKit

class CountriesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
        }
    }
    
    var presenter: CountriesPresenter!
    var router: CountriesRouter!
    var tableDirector: TableDirector!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CountriesConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        setupNavBar()
        presenter.requestData()
    }
    
    func setupNavBar() {
        
        if isRootVC {
        
            let sideBarButton = SideBarButton()
            sideBarButton.didTap = { () in
                self.openLeftMenu()
            }
            navigationItem.leftBarButtonItem = sideBarButton
            
        }
    }
}

extension CountriesViewController: CountriesView {
    
    func display(_ model: [CountyViewModel]) {
        tableDirector.clear()

        let disclosureSection = TableSection()
        let rows = model.flatMap { model -> TableRow<DisclosureIndicatorCell> in
            
            let disclosureAction = TableRowAction<DisclosureIndicatorCell>(.select) { (cell) -> Void in
                
                if model.children.count > 0 {
                    self.router.showCategory(models: model.children.flatMap {
                        return CountyViewModel(id: $0.id ?? "-1", name: $0.name ?? "", children: $0.children, shops: $0.shops)
                        })
                } else if model.children.count <= 0 && model.shops.count > 0 {
                    self.router.showScope(shops: model.shops)
                }
            }
            
            return TableRow<DisclosureIndicatorCell>(item: (title: model.name, detail: "", id: model.id), actions: [disclosureAction])
        }
        disclosureSection.append(rows: rows)
        
        tableDirector += disclosureSection
        tableDirector.reload()
    }
}

