//
//  ItemListPresenter.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

protocol ItemListView: class {
    func display(_ items: ItemList)
}

class ItemListPresenter {
    
    weak var view: ItemListView?
    
    var data: [Item] = [Item]() {
        didSet {
            let list = ItemList(objects: data)
            view?.display(list)
        }
    }
}
