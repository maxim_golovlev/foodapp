import UIKit

enum MenuItemType {
    case scope
    case main
    case favorite
    case about
    case news
    case menu
}

struct MenuItem {
    var title: String
    var selected: Bool = false
    var image: UIImage
    var type: MenuItemType
    
    var controller: UIViewController {
        switch type {
        case .scope:
            let vc = StoryboardScene.Countries.initialViewController()
            if let mainVC = vc.viewControllers.first {
                mainVC.navigationItem.title = title
            }
            return vc
        case .main:
            let vc = StoryboardScene.Main.initialViewController()
            if let mainVC = vc.viewControllers.first as? MainViewController {
                mainVC.navigationItem.title = title
            }
            return vc
        case .favorite:
            let vc = StoryboardScene.Favorite.initialViewController()
            if let mainVC = vc.viewControllers.first as? FavoriteViewController {
                mainVC.navigationItem.title = title
            }
            return vc
        case .about:
            let vc = StoryboardScene.AboutCompany.instantiateAboutCompanyViewController()
            return vc
        case .news:
            let vc = StoryboardScene.News.initialViewController()
            if let mainVC = vc.viewControllers.first as? NewsViewController {
                mainVC.navigationItem.title = title
            }
            return vc
        case .menu:
            let vc = StoryboardScene.MainMenu.initialViewController()
            if let mainVC = vc.viewControllers.first as? MenuViewController {
                mainVC.navigationItem.title = title
            }
            return vc
        }
    }
}
