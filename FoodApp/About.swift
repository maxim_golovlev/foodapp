//
//  About.swift
//  FoodApp
//

import Foundation
import ObjectMapper

class About: Mappable {
    
    var logoUrl: String?
    var text: String?
    var textlink1: String?
    var urllink1: String?
    var textlink2: String?
    var urllink2: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        logoUrl        <- map["logourl"]
        text           <- map["text"]
        textlink1      <- map["textlink1"]
        urllink1       <- map["urllink1"]
        textlink2      <- map["textlink2"]
        urllink2       <- map["urllink2"]
    }
}
