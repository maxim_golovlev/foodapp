import Foundation

class ScopeAPI: MainAPI {
    
    static func getCountries(completion: ServerArrayResult?) {

        sendRequest(type: .get, url: API.countries, parameters: nil, headers: nil, completion: completion)
    }
    
    static func fetchShops(withKey key: String, completion: ServerArrayResult?) {
        
        let params = ["key" : key] as [String: Any]
        
        sendRequest(type: .get, url: API.scopeShops, baseURL: API.scopeURL, parameters: params as [String : AnyObject], headers: nil, completion: completion)
    }
    
    static func fetchCatalog(withSID sid: String?, completion: ServerResult?) {
        
        guard let sid = sid else { return }
        
        let params = ["sid" : sid] as [String: Any]
        
        sendRequest(type: .get, url: API.scopeCatalog, baseURL: API.baseURL, parameters: params as [String : AnyObject], headers: nil, completion: completion)
    }
}
