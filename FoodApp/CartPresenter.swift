import UIKit

protocol CartView: class, BaseView {
    func display(_ models: [String: CardItemModel]?)
}

class CartPresenter {
    
    
    weak var view: CartView?
    
    func requestData() {
       // self.view?.startLoading()
        
        let items = AppCart.sharedCart.items
        self.view?.display(items)
        
    }
  
    
    
}
