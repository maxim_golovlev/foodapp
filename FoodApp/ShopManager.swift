//
//  ShopManager.swift
//  FoodApp
//


import Foundation
import PromiseKit
import MagicalRecord

struct ShopManager {
    
    static func getShop(byId id: Int) -> Promise<Shop> {
        return Promise(resolvers: { (fullfill, reject) in
            
            ShopAPI.getShop(byId: id, completion: { (response) in
                
                switch response {
                case let .Success(response: data):
                    if let shop = Shop(JSON: data) {
                        fullfill(shop)
                    } else {
                        reject(ResponseError.withMessage(AlertMsg.unknownError))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
}
