import Foundation
import Alamofire

protocol MainAPI {
    
    static func sendRequest(type: HTTPMethod, url: String!, baseURL: String, parameters: [String: AnyObject]?, headers: HTTPHeaders?, completion: ServerResult?)
}

extension MainAPI {
    
    static func sendRequest(type: HTTPMethod, url: String!, baseURL: String = API.baseURL, parameters: [String: AnyObject]?, headers: HTTPHeaders?, completion: ServerResult?) {
        
        let urlString = baseURL + url
        
        print(urlString)
        print(parameters ?? "no params")

        Alamofire.request(urlString, method: type, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response)
            
            guard let response = response.result.value as? Dictionary<String, AnyObject> else {
                completion?( .Error (message: "Something wrong. Please try again later."))
                return
            }
            
           if let status = response["success"] as? Bool, status == false {
                print(response["message"] as? String ?? "no message")
                completion?( .Error (message: response["message"] as? String))
            }
    
            completion?( .Success (response: response))
        }
    }
    
    static func sendRequest(type: HTTPMethod, url: String!, baseURL: String = API.baseURL, parameters: [String: AnyObject]?, headers: HTTPHeaders?, completion: ServerArrayResult?) {
        
        let urlString = baseURL + url
        
        print(urlString)
        print(parameters ?? "no params")
        
        Alamofire.request(urlString, method: type, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response)
            
            guard let response = response.result.value as? [Dictionary<String, AnyObject>] else {
                completion?( .Error (message: "Something wrong. Please try again later."))
                return
            }
            
            completion?( .Success (response: response))
        }
    }
}
