
import UIKit

// MARK: - Connect View and Presenter

class FavoriteConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = FavoriteConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: FavoriteViewController) {
        
        let router = FavoriteRouter()
        router.viewController = viewController
        
        let presenter = FavoritePresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
