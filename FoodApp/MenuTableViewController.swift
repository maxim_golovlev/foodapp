//
//  MenuTableViewController.swift
//  tarelochka
//
//  Created by Oleg Melnik on 08.02.17.
//  Copyright © 2017 Oleg Melnik. All rights reserved.
//

import UIKit
import RESideMenu

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Properties.
    
    fileprivate var menuItems = [MenuItem]()

    var side_menu: RESideMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuItems = [
            MenuItem(title: "Выбор ресторана",
                     selected: false,
                     image: UIImage(),
                     type: .scope),
            MenuItem(title: "На главную",
                     selected: true,
                     image: #imageLiteral(resourceName: "main_icon"),
                     type: .main),
            MenuItem(title: "Меню",
                     selected: false,
                     image: #imageLiteral(resourceName: "main_icon"),
                     type: .menu),
            MenuItem(title: "Избранное",
                     selected: false,
                     image: #imageLiteral(resourceName: "main_icon"),
                     type: .favorite),
            MenuItem(title: "Новости и акции",
                     selected: false,
                     image: #imageLiteral(resourceName: "main_icon"),
                     type: .news),
            MenuItem(title: "O компании",
                     selected: false,
                     image: #imageLiteral(resourceName: "main_icon"),
                     type: .about),
        ]
        
        tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
}

extension MenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        let menuItem = menuItems[indexPath.row]
        cell.configure(menuItem: menuItem)
        
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return 150
        default: return 44
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menuItems[indexPath.row]
        for (index, _) in menuItems.enumerated() {
            menuItems[index].selected = indexPath.row == index
        }
        
        tableView.reloadData()
        
        side_menu.setContentViewController(menuItem.controller, animated: true)
        side_menu.hideViewController()
    }
}
