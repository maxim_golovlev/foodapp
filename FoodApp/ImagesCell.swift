
import UIKit
import TableKit

class ImagesCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var colletionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        colletionView.register(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
    }

    fileprivate var dataToDisplay: [String] = [String]() {
        didSet {
            pageControl.numberOfPages = dataToDisplay.count == 1 ? 0 : dataToDisplay.count
            colletionView.reloadData()
        }
    }
    
    static var defaultHeight: CGFloat? {
        return 230
    }
    
    func configure(with imagePaths: [String]) {
        dataToDisplay = imagePaths
    }
    
    @IBAction func didTapLike(_ sender: Any) {
        print("like")
    }
    
    // MARK: - CollectionView Layout.
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = Int(colletionView.contentOffset.x / colletionView.frame.width)
        pageControl.currentPage = currentPage
    }
}

extension ImagesCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        let data = dataToDisplay[indexPath.row]
        
        cell.setImage(data)
        
        return cell
    }
}
