//
//  Info.swift
//  FoodApp
//

import Foundation
import ObjectMapper

class Info: Mappable {

    var id: String?
    var showAbout: Int?
    var companyName: String?
    var companyUrl: String?
    var contactCity: String?
    var scope: String?
    var shopId: Int?
    var contactAddress: String?
    var contactEmail: String?
    var contactPhone: String?
    var sid: String?
    var serviceType: String?
    var logotypeUrl: String?
    var logotypeThumbnail: String?
    var news: [NewsItem] = [NewsItem]()
    var mainImages: [String] = [String]()
    var params: Params?
    var filters: [String] = [String]()
    var addresses: [Adress] = [Adress]()
    var deliveries: [String] = [String]()
    var orderTemplate: [OrderTemplate] = [OrderTemplate]()
    var infoPages: [String] = [String]()
    var about: About?
    var mainScopes: [String] = [String]()

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        showAbout               <- map["show_about"]
        companyName             <- map["company_name"]
        companyUrl              <- map["company_url"]
        contactCity             <- map["contact_city"]
        scope                   <- map["scope"]
        shopId                  <- map["shop_id"]
        contactAddress          <- map["contact_address"]
        contactEmail            <- map["contact_email"]
        contactPhone            <- map["contact_phone"]
        sid                     <- map["sid"]
        serviceType             <- map["service_type"]
        logotypeUrl             <- map["logotype_url"]
        logotypeThumbnail       <- map["logotype_thumbnail"]
        news                    <- map["news"]
        mainImages              <- map["main_images"]
        params                  <- map["params"]
        filters                 <- map["filters"]
        addresses               <- map["addresses"]
        deliveries              <- map["deliveries"]
        orderTemplate           <- map["order_template"]
        infoPages               <- map["info_pages"]
        about                   <- map["about"]
        mainScopes              <- map["main_scopes"]
    }
}
