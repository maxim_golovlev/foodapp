//
//  ScopePresenter.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit
import IGListKit

protocol ScopeView: class, BaseView {
    func display(_ model: [IGListDiffable])
}
/*
class ShopViewModel: NSObject {
    let id: String
    let title: String
    let photoURL: String
    
    init(id: String, title: String, photoURL: String) {
        self.id = id
        self.title = title
        self.photoURL = photoURL
    }
}

extension ShopViewModel: IGListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}*/

class ScopePresenter {
    
    weak var view: ScopeView?
    
    var shops: [ScopeShop]?
    
    func requestData() {
        view?.startLoading()
        
        guard let shops = shops else { view?.stopLoading(); return }

        let shopList = ShopsList(objects: shops)
        
        self.view?.display([shopList])
        view?.stopLoading()
    }
}
