import UIKit

// MARK: - Connect View and Presenter

class CartConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = CartConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: CartViewController) {
        
        let router = CartRouter()
        router.viewController = viewController
        
        let presenter = CartPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
