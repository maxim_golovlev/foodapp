import UIKit

// MARK: - Connect View and Presenter

class CountriesConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = CountriesConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: CountriesViewController) {
        
        let router = CountriesRouter()
        router.viewController = viewController
        
        let presenter = CountriesPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
