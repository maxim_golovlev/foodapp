import UIKit

protocol CountriesView: class, BaseView {
    func display(_ model: [CountyViewModel])
}

struct CountyViewModel {
    let id: String
    let name: String
    let children: [ScopeCategory]
    let shops: [ScopeShop]

}

class CountriesPresenter {
    
    weak var view: CountriesView?
    
    var models: [CountyViewModel]?
    
    func requestData() {
        view?.startLoading()
        
      /*  ScopeManager.getCountires()
            .then { (countries) -> Void in
                let model = countries.flatMap {
                    return CountyViewModel(id: $0.id ?? "-1", name: $0.title ?? "")
                }
                self.view?.display(model)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }*/
        
        if let models = models, models.count > 0 {
            
            self.view?.display(models)
            self.view?.stopLoading()
        } else {
        
            ScopeManager.fetchShops(withKey: "yRQcvZphXZxns73X")
                .then { (categories) -> Void in
                    let model = categories.flatMap {
                        return CountyViewModel(id: $0.id ?? "-1", name: $0.name ?? "", children: $0.children, shops: $0.shops)
                    }
                self.view?.display(model)
                }
                .always {
                    self.view?.stopLoading()
                }
                .catch { (error) in
                    if case let ResponseError.withMessage(msg) = error {
                        self.view?.showAlert(title: nil, message: msg)
                    }
            }
        }
    }
}
