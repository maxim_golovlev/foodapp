import Foundation
import MBProgressHUD
import UIKit

protocol Loadable {
    func startLoading()
    func stopLoading()
}

extension Loadable where Self: UIViewController {
    func startLoading() {
        MBProgressHUD.showAdded(to: view, animated: false)
    }
    
    func stopLoading() {
        MBProgressHUD.hide(for: view, animated: false)
    }
}
