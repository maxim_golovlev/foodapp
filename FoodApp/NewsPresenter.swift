import UIKit

protocol NewsView: class, BaseView {
    func display(_ model: NewsData)
}

class NewsPresenter {
    
    weak var view: NewsView?
  
    
    var news: [NewsItem]? {
        didSet {
            
            if let news = news {
                self.view?.display(NewsData(title: nil, objects: news))
            }
        }
    }
    
    func requestData() {
        self.view?.startLoading()
        
        ScopeManager.fetchCatalog()
            .then { (catalog) -> Void in
                
                let news = NewsData(title: "Новости и акции (\(catalog.info?.news.count ?? 0))",
                    objects: catalog.info?.news ?? [NewsItem]())
                
                self.view?.display(news)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
}
