//
//  NewsItemPresenter.swift
//  FoodApp
//
//  Created by Admin on 18.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import UIKit

protocol NewsItemView: class, BaseView {
    func display(_ model: NewsItem)
}

class NewsItemPresenter {
    
    weak var view: NewsItemView?
    
    
    var newsItem: NewsItem?
    
    func requestData() {
        
        if let newsItem = newsItem {
            self.view?.display(newsItem)
        }
    }
}
