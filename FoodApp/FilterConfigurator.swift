
import UIKit

// MARK: - Connect View and Presenter

class FilterConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = FilterConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: FilterViewController) {
        
        let router = FilterRouter()
        router.viewController = viewController
        
        let presenter = FilterPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
