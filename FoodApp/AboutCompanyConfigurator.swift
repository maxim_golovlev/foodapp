
import UIKit

// MARK: - Connect View and Presenter

class AboutCompanyConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = AboutCompanyConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: AboutCompanyViewController) {
        
        let router = AboutCompanyRouter()
        router.viewController = viewController
        
        let presenter = AboutCompanyPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
