
import Foundation

extension Error {
    
}

enum ResponseError: Error {
    case withMessage(String?)
}

enum SuccessResponse {
    case Success
    case Error (message: String)
}
