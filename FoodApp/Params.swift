//
//  Params.swift
//  FoodApp
//

import Foundation
import ObjectMapper

class Params: Mappable {
    
    var buttonColor: String? {
        didSet {
            
            guard let hexColor = navbarColor else { return }
            
            let color = UIColor.hexStringToUIColor(hex: hexColor)
            
            AppColors.buttonsColor = color
        }
    }
    var cardBackUrl: String?
    var cardFrontUrl: String?
    var deliveryCost: String?
    var deliveryTime: String?
    var displayCountInCategory: Int?
    var displayPhotoOfCategory: Int?
    var homeBlock: [HomeBlock] = [HomeBlock]()
    var loyaltySid: String?
    var loyaltyUrl: String?
// menu block
    var minimalOrder: Int?
    var navbarColor: String? {
        didSet {
            
            guard let hexColor = navbarColor else { return }
            
            let color = UIColor.hexStringToUIColor(hex: hexColor)
            
           AppColors.navBarColor = color
            
        }
    }
    var orderEmailText: String?
    var orderSuccessText: String?
    var sendEmailToBuyer: Int?
    var specificationDescription: String?
    var tabbarColor: String?
    var orderDiscount: Int?
    var allowOutOfStock: Int?
    var doNotDisplayPrices: Int?
    var orderMailItemsExtData: Int?
    var groupItemsOnApi: Int?
    var defaultCurrency: String?
    var defaultLanguage: String?
    var basket: Int?
    var tile: Int?
    var standard: Int?
    var consultant: Int?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        cardBackUrl                 <- map["card_back"]
        cardFrontUrl                <- map["card_front"]
        loyaltySid                  <- map["loyalty_sid"]
        loyaltyUrl                  <- map["loyalty_url"]
        
        navbarColor                  <- map["navbar_color"]
        tabbarColor                  <- map["tabbar_color"]
        buttonColor                  <- map["button_color"]
        minimalOrder                 <- map["minimal_order"]
        orderDiscount                <- map["order_discount"]
        allowOutOfStock              <- map["allow_out_of_stock"]
        doNotDisplayPrices           <- map["do_not_display_prices"]
        displayCountInCategory       <- map["display_count_in_category"]
        displayPhotoOfCategory       <- map["display_photo_of_category"]
        orderMailItemsExtData        <- map["order_mail_items_ext_data"]
        groupItemsOnApi              <- map["group_items_on_api"]
        sendEmailToBuyer             <- map["send_email_to_buyer"]
        defaultCurrency              <- map["default_currency"]
        defaultLanguage              <- map["default_language"]
        orderSuccessText             <- map["order_success_text"]
        orderEmailText               <- map["order_email_text"]
        specificationDescription     <- map["specification_description"]
        deliveryCost                 <- map["delivery_cost"]
        deliveryTime                 <- map["delivery_time"]
        basket                       <- map["basket"]
        tile                         <- map["tile"]
        standard                     <- map["standard"]
        consultant                   <- map["consultant"]
        homeBlock                    <- map["home_block"]

    }
}
