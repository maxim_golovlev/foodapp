//
//  ItemCell.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright © 2017 Захаров Вячеслав. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {

    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.layer.borderColor = AppColors.buttonsColor.cgColor
            let image = #imageLiteral(resourceName: "cart_icon_small").withRenderingMode(.alwaysTemplate)
            
            addButton.setImage(image, for: .normal)
            addButton.tintColor = AppColors.buttonsColor
        }
    }
    
    @IBOutlet weak var stepper: GMStepper! {
        didSet {
            stepper.buttonsBackgroundColor = AppColors.buttonsColor
        }
    }
    
    var price: Int? {
        didSet {
            
            let priceStr = "\(price ?? 0)"
            
            addButton.setTitle(priceStr, for: .normal)
            stepper.price = priceStr
        }
    }
    
    var item: Item? {
        didSet {
            
            
            
        }
    }
    
    var initCounter: Int? = 0 {
        didSet {
            
            guard let counter = initCounter else { return }
            
            if counter > 0 {
                addButton.isHidden = true
                stepper.isHidden = false
                stepper.value = Double(counter)
            }
        }
    }
    
    var counter: Int? = 0 {
        didSet {
            
            guard let counter = counter else { return }
            
            AppCart.sharedCart.create(number: counter, item: item)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        addButton.isHidden = false
        stepper.isHidden = true
        item = nil
        stepper.value = 0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func didTapAdd(_ sender: Any) {
        addButton.isHidden = true
        stepper.isHidden = false
        stepper.value = 1.0
    }
    
    @IBAction func countChanged(_ sender: GMStepper) {
        
        counter = Int(sender.value)
        
        if sender.value == 0 {
            addButton.isHidden = false
            stepper.isHidden = true
        }
    }
}
