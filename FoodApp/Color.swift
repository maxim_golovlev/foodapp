import Foundation
import UIKit

struct AppColors {
    
    
    
    static let defaultColor = #colorLiteral(red: 0.4549019608, green: 0.7254901961, blue: 0.3529411765, alpha: 1)
    static let separatorColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
    static let buttonNotif = NSNotification.Name(rawValue: "ButtonColorNotif")
    
    static var navBarColor: UIColor = defaultColor
    static var buttonsColor: UIColor = defaultColor {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: buttonNotif, object: nil, userInfo: nil)
            }
        }
    }
}
