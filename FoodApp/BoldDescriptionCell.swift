
import UIKit
import TableKit

class BoldDescriptionCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 47.5
    }
    
    func configure(with data: (title: String, detail: String)) {
        titleLabel.text = data.title
        detailLabel.text = data.detail
    }
}
