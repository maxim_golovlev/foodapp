import UIKit

class DismissBarButton: UIBarButtonItem {
    
    var didTap: (() -> ())?
    
    // MARK: - Inits.
    
    override init() {
        super.init()
        
        self.image = #imageLiteral(resourceName: "close_button")
        self.target = target
        self.action = #selector(didTapOn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension DismissBarButton {
    
    func didTapOn() {
        didTap?()
    }
}
