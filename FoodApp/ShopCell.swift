//
//  ShopCell.swift
//  FoodApp
//
//  Created by Anton Vodolazkyi on 10.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import UIKit

class ShopCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
