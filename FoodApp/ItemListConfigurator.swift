//
//  ItemListConfigurator.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

// MARK: - Connect View and Presenter

class ItemListConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = ItemListConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: ItemListViewController) {
        
        let router = ItemListRouter()
        router.viewController = viewController
        
        let presenter = ItemListPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
