
import UIKit
import TableKit

class FilterViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
        }
    }
    
    var presenter: FilterPresenter!
    var router: FilterRouter!
    var tableDirector: TableDirector!

    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FilterConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        displayData()
    }
    
    @IBAction func didTapClear(_ sender: Any) {
        
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension FilterViewController: FilterView {
    
    func displayData() {
        tableDirector.clear()

        let disclosureSection = TableSection()
        let disclosureRow = TableRow<DisclosureIndicatorCell>(item: (title: "Размер пиццы", detail: "Все", id: nil))
        disclosureSection.append(row: disclosureRow)
        let disclosureRow1 = TableRow<DisclosureIndicatorCell>(item: (title: "Размер пиццы", detail: "35 см.", id: nil))
        disclosureSection.append(row: disclosureRow1)
        let disclosureRow2 = TableRow<DisclosureIndicatorCell>(item: (title: "Размер пиццы", detail: "Выбрано 3", id: nil))
        disclosureSection.append(row: disclosureRow2)
        
        tableDirector += disclosureSection
        tableDirector.reload()
    }
}

