//
//  ScopeCategory.swift
//  foodapp
//
//  Created by Admin on 17.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import Foundation
import ObjectMapper

class ScopeCategory: Mappable {
    
    var id: String?
    var name: String?
    var type: String?
    var children: [ScopeCategory] = [ScopeCategory]()
    var shops: [ScopeShop] = [ScopeShop]()
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
        type        <- map["type"]
        children    <- map["children"]
        shops       <- map["shops"]
    }
}
