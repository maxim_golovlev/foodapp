//
//  ItemConfigurator.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 15.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

// MARK: - Connect View and Presenter

class ItemConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = ItemConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: ItemViewController) {
        
        let router = ItemRouter()
        router.viewController = viewController
        
        let presenter = ItemPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
