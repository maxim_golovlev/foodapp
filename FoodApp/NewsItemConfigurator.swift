//
//  NewsItemConfigurator.swift
//  FoodApp
//
//  Created by Admin on 18.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import UIKit

// MARK: - Connect View and Presenter

class NewsItemConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = NewsItemConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: NewsItemController) {
        
     /*   let router = NewsItemRouter()
        router.viewController = viewController*/
        
        let presenter = NewsItemPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
      //  viewController.router = router
    }
}
