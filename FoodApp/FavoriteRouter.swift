
import UIKit

class FavoriteRouter {
    
    weak var viewController: FavoriteViewController!
    
    // MARK: - Navigation
    
    func presentFilter() {
        let vc = StoryboardScene.Filter.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
    
    func presentCart() {
        let vc = StoryboardScene.Cart.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
}
