//
//  Storyboards.swift
//  FoodApp
//

import Foundation

import UIKit

protocol StoryboardSceneType {
    static var storyboardName: String { get }
}

extension StoryboardSceneType {
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.storyboardName, bundle: nil)
    }
    
    static func initialViewController() -> UIViewController {
        guard let vc = storyboard().instantiateInitialViewController() else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
    func viewController() -> UIViewController {
        return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
    }
    static func viewController(identifier: Self) -> UIViewController {
        return identifier.viewController()
    }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
    func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
        performSegue(withIdentifier: segue.rawValue, sender: sender)
    }
}

struct StoryboardScene {
    enum LaunchScreen: StoryboardSceneType {
        static let storyboardName = "LaunchScreen"
    }
    enum MenuViewController: String, StoryboardSceneType {
        static let storyboardName = "MenuViewController"
        
        case menuViewControllerScene = "MenuViewController"
        static func instantiateMenuViewController() -> FoodApp.MenuViewController {
            guard let vc = StoryboardScene.MenuViewController.menuViewControllerScene.viewController() as? FoodApp.MenuViewController
                else {
                    fatalError("ViewController 'MenuViewController' is not of the expected class FoodApp.MenuViewController.")
            }
            return vc
        }
    }
    
    enum MainMenu: String, StoryboardSceneType {
        static let storyboardName = "MainMenu"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case mainMenuViewControllerScene = "MainMenuViewController"
        static func instantiateMainMenuViewController() -> FoodApp.MainMenuViewController {
            guard let vc = StoryboardScene.MainMenu.mainMenuViewControllerScene.viewController() as? FoodApp.MainMenuViewController
                else {
                    fatalError("ViewController 'MainMenuViewController' is not of the expected class FoodApp.MainMenuViewController.")
            }
            return vc
        }
    }
    
    enum ItemList: String, StoryboardSceneType {
        static let storyboardName = "ItemList"
        
        case itemListViewControllerScene = "ItemListViewController"
        static func instantiateItemListViewController() -> FoodApp.ItemListViewController {
            guard let vc = StoryboardScene.ItemList.itemListViewControllerScene.viewController() as? FoodApp.ItemListViewController
                else {
                    fatalError("ViewController 'ItemListViewController' is not of the expected class FoodApp.ItemListViewController.")
            }
            return vc
        }
    }
    
    enum Main: String, StoryboardSceneType {
        static let storyboardName = "Main"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case mainViewControllerScene = "MainViewController"
        static func instantiateMainViewController() -> FoodApp.MainViewController {
            guard let vc = StoryboardScene.Main.mainViewControllerScene.viewController() as? FoodApp.MainViewController
                else {
                    fatalError("ViewController 'MainViewController' is not of the expected class FoodApp.MainViewController.")
            }
            return vc
        }
    }
    
    enum Scope: String, StoryboardSceneType {
        static let storyboardName = "Scope"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case scopeViewControllerScene = "ScopeViewController"
        static func instantiateScopeViewController() -> FoodApp.ScopeViewController {
            guard let vc = StoryboardScene.Scope.scopeViewControllerScene.viewController() as? FoodApp.ScopeViewController
                else {
                    fatalError("ViewController 'ScopeViewController' is not of the expected class FoodApp.ScopeViewController.")
            }
            return vc
        }
    }
    
    enum Item: String, StoryboardSceneType {
        static let storyboardName = "Item"
        
        case itemViewControllerScene = "ItemViewController"
        static func instantiateItemViewController() -> FoodApp.ItemViewController {
            guard let vc = StoryboardScene.Item.itemViewControllerScene.viewController() as? FoodApp.ItemViewController
                else {
                    fatalError("ViewController 'ItemViewController' is not of the expected class FoodApp.ItemViewController.")
            }
            return vc
        }
    }
    
    enum NewsItem: String, StoryboardSceneType {
        static let storyboardName = "NewsItem"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case itemViewControllerScene = "NewsItemViewController"
        static func instantiateItemViewController() -> FoodApp.NewsItemController {
            guard let vc = StoryboardScene.Item.itemViewControllerScene.viewController() as? FoodApp.NewsItemController
                else {
                    fatalError("ViewController 'ItemViewController' is not of the expected class FoodApp.NewsItemController.")
            }
            return vc
        }
    }
    
    enum Favorite: String, StoryboardSceneType {
        static let storyboardName = "Favorite"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case favoriteViewControllerScene = "FavoriteViewController"
        static func instantiateFavoriteViewController() -> FoodApp.FavoriteViewController {
            guard let vc = StoryboardScene.Favorite.favoriteViewControllerScene.viewController() as? FoodApp.FavoriteViewController
                else {
                    fatalError("ViewController 'FavoriteViewController' is not of the expected class FoodApp.FavoriteViewController.")
            }
            return vc
        }
    }
    
    enum Filter: String, StoryboardSceneType {
        static let storyboardName = "Filter"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case filterViewControllerScene = "FilterViewController"
        static func instantiateFilterViewController() -> FoodApp.FilterViewController {
            guard let vc = StoryboardScene.Filter.filterViewControllerScene.viewController() as? FoodApp.FilterViewController
                else {
                    fatalError("ViewController 'FilterViewController' is not of the expected class FoodApp.FilterViewController.")
            }
            return vc
        }
    }
    
    enum AboutCompany: String, StoryboardSceneType {
        static let storyboardName = "AboutCompany"
        
        case aboutCompanyViewControllerScene = "AboutCompanyViewController"
        static func instantiateAboutCompanyViewController() -> FoodApp.AboutCompanyViewController {
            guard let vc = StoryboardScene.AboutCompany.aboutCompanyViewControllerScene.viewController() as? FoodApp.AboutCompanyViewController
                else {
                    fatalError("ViewController 'AboutCompanyViewController' is not of the expected class FoodApp.AboutCompanyViewController.")
            }
            return vc
        }
    }
    
    enum News: String, StoryboardSceneType {
        static let storyboardName = "News"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case newsViewControllerScene = "NewsViewController"
        static func instantiateNewsViewController() -> FoodApp.NewsViewController {
            guard let vc = StoryboardScene.News.newsViewControllerScene.viewController() as? FoodApp.NewsViewController
                else {
                    fatalError("ViewController 'NewsViewController' is not of the expected class FoodApp.NewsViewController.")
            }
            return vc
        }
    }
    
    enum Countries: String, StoryboardSceneType {
        static let storyboardName = "Countries"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case countriesViewControllerScene = "CountriesViewController"
        static func instantiateCountriesViewController() -> FoodApp.CountriesViewController {
            guard let vc = StoryboardScene.Countries.countriesViewControllerScene.viewController() as? FoodApp.CountriesViewController
                else {
                    fatalError("ViewController 'CountriesViewController' is not of the expected class FoodApp.CountriesViewController.")
            }
            return vc
        }
    }
    
    enum Cart: String, StoryboardSceneType {
        static let storyboardName = "Cart"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case cartViewControllerScene = "CartViewController"
        static func instantiateCartViewController() -> FoodApp.CartViewController {
            guard let vc = StoryboardScene.Cart.cartViewControllerScene.viewController() as? FoodApp.CartViewController
                else {
                    fatalError("ViewController 'CartViewController' is not of the expected class FoodApp.CartViewController.")
            }
            return vc
        }
    }
    
    enum Checkout: String, StoryboardSceneType {
        static let storyboardName = "Checkout"
        
        static func initialViewController() -> UINavigationController {
            guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
                fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
            }
            return vc
        }
        
        case checkoutViewControllerScene = "CheckoutViewController"
        static func instantiateCheckoutViewController() -> FoodApp.CheckoutViewController {
            guard let vc = StoryboardScene.Checkout.checkoutViewControllerScene.viewController() as? FoodApp.CheckoutViewController
                else {
                    fatalError("ViewController 'CheckoutViewController' is not of the expected class FoodApp.CheckoutViewController.")
            }
            return vc
        }
    }
}
