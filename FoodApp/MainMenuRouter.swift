//
//  MainMenuRouter.swift
//  FoodApp
//

import UIKit

class MainMenuRouter {
    
    weak var viewController: MainMenuViewController!
    
    // MARK: - Navigation
    
    func showItemList(withCategoryId id: Int?, title: String?) {
        let itemListVc = StoryboardScene.ItemList.instantiateItemListViewController()
        itemListVc.presenter.categoryId = id
        itemListVc.navigationItem.title = title
        viewController?.navigationController?.show(itemListVc, sender: nil)
    }
    
    func presentCart() {
        let vc = StoryboardScene.Cart.initialViewController()
        viewController.present(vc, animated: true, completion: nil)
    }
}
