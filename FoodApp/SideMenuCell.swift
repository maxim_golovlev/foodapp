import UIKit

class SideMenuCell: UITableViewCell {

    // MARK: - Outlets.

    @IBOutlet weak var l_title: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    
    var selectedItem = false {
        didSet {
            backgroundImageView.backgroundColor = selectedItem ? AppColors.buttonsColor : UIColor.clear
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateColor), name: AppColors.buttonNotif, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateColor() {
        backgroundImageView.backgroundColor = selectedItem ? AppColors.buttonsColor : UIColor.clear
    }

    // MARK: - Configuration.

    func configure(menuItem: MenuItem) {
        l_title.text = menuItem.title
        selectedItem = menuItem.selected
        logoImage.image = menuItem.image
    }
}
