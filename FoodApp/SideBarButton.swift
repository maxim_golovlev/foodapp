import UIKit

class SideBarButton: UIBarButtonItem {
    
    var didTap: (() -> ())?
    
    // MARK: - Inits.
    
    override init() {
        
     /*   let button = UIButton(type: .system)
        button.frame = CGRectMake(0, 0, 40, 40)
        button.layer.cornerRadius = 20
        button.layer.masksToBounds = true
        button.backgroundColor = .white
        button.setBackgroundImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = AppColors.buttonsColor
        
        super.init(customView: button)*/
        super.init()
        
        self.image = #imageLiteral(resourceName: "Full_Menu").withRenderingMode(.alwaysOriginal)
        self.tintColor = AppColors.buttonsColor
        self.target = target
        self.action = #selector(didTapOn)
     //   self.setBackgroundImage(#imageLiteral(resourceName: "white_circle"), for: .normal, barMetrics: .default)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension SideBarButton {
    
    func didTapOn() {
        didTap?()
    }
}

