
import UIKit

// MARK: - Connect View and Presenter

class NewsConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = NewsConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: NewsViewController) {
        
        let router = NewsRouter()
        router.viewController = viewController
        
        let presenter = NewsPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
