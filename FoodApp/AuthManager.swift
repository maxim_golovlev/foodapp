//
//  AuthManager.swift
//  FoodApp
//

import Foundation
import PromiseKit
import MagicalRecord

struct AuthManager {
    
    static func initMethod() -> Promise<User?> {
        return Promise(resolvers: { (fullfill, reject) in
            
            let action: Action = User.current == nil ? .set : .get
            AuthAPI.initMethod(action: action) { (response) in
                
                switch response {
                case let .Success(response: data):
                    var newUser: User?
                    
                    MagicalRecord.save({ (localContext) in
                        newUser = User.mr_createEntity(in: localContext)
                        newUser?.stamp = data["stamp"] as? String
                    }) { (success, error) in
                        if success {
                            fullfill(newUser)
                        } else {
                            reject(ResponseError.withMessage(AlertMsg.unknownError))
                        }
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            }
        })
    }
}
