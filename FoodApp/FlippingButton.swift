//
//  FlippingButton.swift
//  FoodApp
//
//  Created by Admin on 18.04.17.
//  Copyright © 2017 Антон Водолазский. All rights reserved.
//

import UIKit


class FlippingButton: UIButton {
    
    let flipTime = 0.3
    var isFlipped = false
    
    var frontImageUrl: String? {
        didSet {
            toggleImage()
        }
    }
    var backImageUrl: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }

    func setup() {
        
        self.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
    }
    
    func handleTap() {
        flip()
    }
    
    func flip() {
        
        UIView.transition(with: self,
                          duration: flipTime,
                          options: [.curveEaseOut, .transitionFlipFromRight] ,
                          animations: {
                            self.toggleImage()
                            
                        }, completion: nil)
    }
    
    func toggleImage() {
        let imageUrl = self.isFlipped ? self.backImageUrl : self.frontImageUrl
        let url = URL(string: imageUrl ?? "")
        self.sd_setBackgroundImage(with: url, for: .normal)
        self.isFlipped = !self.isFlipped
    }
}
