import UIKit
import IGListKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var collectionVIew: IGListCollectionView!
    
    var presenter: NewsPresenter!
    var router: NewsRouter!
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    var data = [IGListDiffable]()
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NewsConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: - View lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        setupNavBar()
        
        collectionVIew.register(UINib(nibName: "NewsListCell", bundle: nil), forCellWithReuseIdentifier: "NewsListCell")
        adapter.collectionView = collectionVIew
        adapter.dataSource = self
        
        if isRootVC {
            presenter.requestData()
        }
    }
    
    func setupNavBar() {
        let sideBarButton = SideBarButton()
        sideBarButton.didTap = { () in
            self.openLeftMenu()
        }
        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = sideBarButton
        }
    }
}

extension NewsViewController: NewsView {
    
    func display(_ newsData: NewsData) {
        data = [newsData]
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension NewsViewController: IGListAdapterDataSource {
    
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return data
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        return NewsSectionController(withType: .withoutHeader)
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
}

