//
//  ScopeConfigurator.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

// MARK: - Connect View and Presenter

class ScopeConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = ScopeConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: ScopeViewController) {
        
        let router = ScopeRouter()
        router.viewController = viewController
        
        let presenter = ScopePresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
