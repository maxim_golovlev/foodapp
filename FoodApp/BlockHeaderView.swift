//
//  BlockHeaderView.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright © 2017 Захаров Вячеслав. All rights reserved.
//

import UIKit

class BlockHeaderView: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    var tapOn: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func didTapOnHeader(_ sender: UIButton) {
        tapOn?()
    }
}
