//
//  MainConfigurator.swift
//  FoodApp
//
//  Created by Захаров Вячеслав on 14.03.17.
//  Copyright (c) 2017 Захаров Вячеслав. All rights reserved.


import UIKit

// MARK: - Connect View and Presenter

class MainConfigurator {
    
    // MARK: - Object lifecycle
    
    static let sharedInstance = MainConfigurator()
    
    private init() {}
    
    // MARK: - Configuration
    
    func configure(viewController: MainViewController) {
        
        let router = MainRouter()
        router.viewController = viewController
        
        let presenter = MainPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.router = router
    }
}
