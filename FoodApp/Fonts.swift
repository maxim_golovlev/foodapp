import UIKit.UIFont
typealias Font = UIFont


protocol FontConvertible {
    func font(size: CGFloat) -> Font!
}

extension FontConvertible where Self: RawRepresentable, Self.RawValue == String {
    func font(size: CGFloat) -> Font! {
        return Font(font: self, size: size)
    }
}

extension Font {
    convenience init!<FontType: FontConvertible>
        (font: FontType, size: CGFloat)
        where FontType: RawRepresentable, FontType.RawValue == String {
            self.init(name: font.rawValue, size: size)
    }
}

struct FontFamily {
    enum SFUIDisplay: String, FontConvertible {
        case regular = "SFUIDisplay-Regular"
        case semibold = "SFUIDisplay-Semibold"
        case bold = "SFUIDisplay-Bold"
        case medium = "SFUIDisplay-Medium"
        case light = "SFUIDisplay-Light"
    }
    
    enum SFUIText: String, FontConvertible {
        case regular = "SFUIText-Regular"
        case bold = "SFUIText-Bold"
        case light = "SFUIText-Light"
        case medium = "SFUIText-Medium"
        case semibold = "SFUIText-Semibold"
    }
}
